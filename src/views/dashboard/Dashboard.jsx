import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Pagination from "@material-ui/lab/Pagination";
import "./Dashboard.css"
import CircularProgress from '@material-ui/core/CircularProgress';



const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function Dashboard() {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [lim, setLim] = useState(5);
  const [off, setOff] = useState(1);
  const [totalPage, setTotalPage] = useState(1)
  const [dataFetched, setDataFetched] = useState(false)
  const[edit,setEdit]= useState("");

  const getDatas = async (limit, offset) => {
    try {
      setDataFetched(false)// error (gagal get data article posts)
      const res = await axios.get(`$(postsURL)` + limit + `/` + offset);
      if (res.status === 200) {
        setData(res.data.data);
        setDataFetched(true)
        //console.log(res.data)
        setTotalPage(Math.ceil(res.data.total / lim))
      }

    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getDatas(lim, off);

  }, [])

  function changePage(page) {
    getDatas(lim, page);
  }


  return (
    <div>
      <h1>Article Posts</h1>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Id</StyledTableCell>
              <StyledTableCell>Title</StyledTableCell>
              <StyledTableCell>Category</StyledTableCell>
              <StyledTableCell>Action</StyledTableCell>
            </TableRow>
          </TableHead>
          {!dataFetched ? <CircularProgress /> : <TableBody>
            {data.map((item) => {
              return (
                <StyledTableRow key={item.id}>
                <StyledTableCell component="th" scope="row">
                    {item.id}
                  </StyledTableCell>
                  <StyledTableCell>
                    {item.title}
                  </StyledTableCell>
                  <StyledTableCell>
                    {item.category}
                  </StyledTableCell>
                  <StyledTableCell>
                  <button onClick={edit} className="btn btn-warning mr-2">Edit</button>

                  <button className="btn btn-danger">Trash</button>
                  </StyledTableCell>
                </StyledTableRow>
              );
            })}
          </TableBody>}

        </Table>
      </TableContainer>
      <Pagination count={totalPage} onChange={(event, val) => changePage(val)} size='large' />
    </div>
  )
}
