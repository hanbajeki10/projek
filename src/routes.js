import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard.jsx'));
const Create = React.lazy(() => import('./views/create/AddNews.jsx'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/Article-Posts', name: '', component: Dashboard, exact: true},
  { path: '/Article-Posts/create', name: 'Create Article Posts', component: Create},
 
];

export default routes;
